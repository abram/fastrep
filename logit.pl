#!/usr/bin/perl
use strict;
my $row;
my ($inputfile, $column, $lmfit,$NOREP,$ALLCONTEXT) = @ARGV;
$lmfit ||= "lmfit-coef.csv";
$column ||= "cosine_architecture";
$inputfile ||= "header_eclipse_cosines.csv";
$NOREP ||= 0;
$ALLCONTEXT ||= 0;

warn join("\t",$inputfile, $column, $lmfit,$NOREP,$ALLCONTEXT);


# get the coeffecients

open my $fh, "<:encoding(utf8)", "lmfit-coef.csv" or die "test.csv: $!";
my @b = ();
my $first=0;
my @row;
while (  $row = <$fh> ) {
    chomp($row);
    @row = split(/,/,$row);
    push @b, $row[1] unless !$first++;
}
close $fh;
warn join("\t",@b);
# REP ONLY?
if (@b == 2) {
    $b[2] = 0.0;
}
# this is complicated
# basically we should listent to the coeffecients file
# but in the case of NOREP alone we still have REP in our equation
# but we cancel it by * 0
# If we have NO REP and ALLCONTEXT we have to zero out rep by inserting it
if ($NOREP) {
    my ($first,@rest) = @b;
    @b = ($first,0.0,@rest);
}

# premature evaluation
# this got complicated
# makes the z function for the logistic regression
if ($ALLCONTEXT) {
    my @strs = ();
    for (my $i = 1; $i < @b; $i++) {
        my $j = $i - 1;
        push @strs, "$b[$i] * \$_[$j]";
    }
    my $code = "sub z {  $b[0] + ".join(" + ", @strs)." } ";
    warn $code;
    eval $code;
    #eval "sub z {  $b[0] + $b[1] * \$_[0] + $b[2] * \$_[1] }";
} else {
    eval "sub z {  $b[0] + $b[1] * \$_[0] + $b[2] * \$_[1] }";
}
warn z(0,1);
warn z(1,0);
warn z(0,0);

# now 

open my $fh, "<:encoding(utf8)", $inputfile or die "$inputfile: $!";
my $header = <$fh>;
chomp($header);
my @header = split(/,/,$header);
#print join(",",@header,"logit"),$/;
my %header = ();
{
    my $i = 0;
    %header = map { $_ => $i++; } @header;
}
my $repc = $header{REP};
my @h = ($repc);
if ($ALLCONTEXT) {
    push @h, map {$header{$_}} qw(cosine_architecture cosine_nfr cosine_lda);
} else {
    die "UNDEFINED COLUMN $column" unless exists $header{$column};
    my $c = $header{$column};
    push @h, $c;
}



warn "HEADERS: ".join("\t",@h);

sub logit { exp($_[0])/(1+exp($_[0])) }

while ( $row = <$fh> ) {
    chomp($row);
    @row = split(/,/,$row);
    print join(",",$row, logit(z(map{$row[$_]}@h))),$/;
}
