# Using Mozilla and Eclipse generate Map Data

# CSVs with loggit column

eclipse_rep.csv: header_eclipse_cosines.csv
	time bash -x mklogit.sh header_eclipse_cosines.csv eclipse_rep.csv REP

eclipse_all.csv: header_eclipse_cosines.csv
	time bash -x mklogit.sh header_eclipse_cosines.csv eclipse_all.csv ALL 0 1

eclipse_all_norep.csv: header_eclipse_cosines.csv
	time bash -x mklogit.sh header_eclipse_cosines.csv eclipse_all_norep.csv ALL 1 1

eclipse_cosines_arch.csv: header_eclipse_cosines.csv 
	time bash -x mklogit.sh header_eclipse_cosines.csv eclipse_cosines_arch.csv cosine_architecture
eclipse_cosines_lda.csv: header_eclipse_cosines.csv 
	time bash -x mklogit.sh header_eclipse_cosines.csv eclipse_cosines_lda.csv cosine_lda
eclipse_cosines_junk.csv: header_eclipse_cosines.csv 
	time bash -x mklogit.sh header_eclipse_cosines.csv eclipse_cosines_junk.csv cosine_junk
eclipse_cosines_nfr.csv: header_eclipse_cosines.csv 
	time bash -x mklogit.sh header_eclipse_cosines.csv eclipse_cosines_nfr.csv cosine_nfr

eclipse_norep_arch.csv: header_eclipse_cosines.csv 
	time bash -x mklogit.sh header_eclipse_cosines.csv eclipse_norep_arch.csv cosine_architecture 1
eclipse_norep_lda.csv: header_eclipse_cosines.csv 
	time bash -x mklogit.sh header_eclipse_cosines.csv eclipse_norep_lda.csv cosine_lda 1
eclipse_norep_junk.csv: header_eclipse_cosines.csv 
	time bash -x mklogit.sh header_eclipse_cosines.csv eclipse_norep_junk.csv cosine_junk 1
eclipse_norep_nfr.csv: header_eclipse_cosines.csv 
	time bash -x mklogit.sh header_eclipse_cosines.csv eclipse_norep_nfr.csv cosine_nfr 1



mozilla_cosines_arch.csv: header_mozilla_cosines.csv
	time bash -x mklogit.sh header_mozilla_cosines.csv mozilla_cosines_arch.csv cosine_architecture
mozilla_cosines_lda.csv: header_mozilla_cosines.csv
	time bash -x mklogit.sh header_mozilla_cosines.csv mozilla_cosines_lda.csv cosine_lda
mozilla_cosines_junk.csv: header_mozilla_cosines.csv
	time bash -x mklogit.sh header_mozilla_cosines.csv mozilla_cosines_junk.csv cosine_junk
mozilla_cosines_nfr.csv: header_mozilla_cosines.csv
	time bash -x mklogit.sh header_mozilla_cosines.csv mozilla_cosines_nfr.csv cosine_nfr

mozilla_norep_arch.csv: header_mozilla_cosines.csv 
	time bash -x mklogit.sh header_mozilla_cosines.csv mozilla_norep_arch.csv cosine_architecture 1
mozilla_norep_lda.csv: header_mozilla_cosines.csv 
	time bash -x mklogit.sh header_mozilla_cosines.csv mozilla_norep_lda.csv cosine_lda 1
mozilla_norep_junk.csv: header_mozilla_cosines.csv 
	time bash -x mklogit.sh header_mozilla_cosines.csv mozilla_norep_junk.csv cosine_junk 1
mozilla_norep_nfr.csv: header_mozilla_cosines.csv 
	time bash -x mklogit.sh header_mozilla_cosines.csv mozilla_norep_nfr.csv cosine_nfr 1

mozilla_all.csv: header_mozilla_cosines.csv
	time bash -x mklogit.sh header_mozilla_cosines.csv mozilla_all.csv ALL 0 1

mozilla_all_norep.csv: header_mozilla_cosines.csv
	time bash -x mklogit.sh header_mozilla_cosines.csv mozilla_all_norep.csv ALL 1 1



# list of dupes

mozilla.dupes: header_mozilla_cosines.csv getdups.sh
	bash getdups.sh header_mozilla_cosines.csv mozilla.dupes

eclipse.dupes: header_eclipse_cosines.csv getdups.sh
	bash getdups.sh header_eclipse_cosines.csv eclipse.dupes

# map results

eclipse_rep_map.txt: eclipse.dupes eclipse_rep.csv 
	time perl aveP.pl eclipse.dupes < eclipse_rep.csv | tee eclipse_rep_map.txt

eclipse_cosine_arch_map.txt: eclipse.dupes eclipse_cosines_arch.csv 
	time perl aveP.pl eclipse.dupes < eclipse_cosines_arch.csv | tee eclipse_cosine_arch_map.txt
eclipse_cosine_lda_map.txt: eclipse.dupes eclipse_cosines_lda.csv 
	time perl aveP.pl eclipse.dupes < eclipse_cosines_lda.csv | tee eclipse_cosine_lda_map.txt
eclipse_cosine_junk_map.txt: eclipse.dupes eclipse_cosines_junk.csv 
	time perl aveP.pl eclipse.dupes < eclipse_cosines_junk.csv | tee eclipse_cosine_junk_map.txt
eclipse_cosine_nfr_map.txt: eclipse.dupes eclipse_cosines_nfr.csv 
	time perl aveP.pl eclipse.dupes < eclipse_cosines_nfr.csv | tee eclipse_cosine_nfr_map.txt

eclipse_norep_arch_map.txt: eclipse.dupes eclipse_norep_arch.csv 
	time perl aveP.pl eclipse.dupes < eclipse_norep_arch.csv | tee eclipse_norep_arch_map.txt
eclipse_norep_lda_map.txt: eclipse.dupes eclipse_norep_lda.csv 
	time perl aveP.pl eclipse.dupes < eclipse_norep_lda.csv | tee eclipse_norep_lda_map.txt
eclipse_norep_junk_map.txt: eclipse.dupes eclipse_norep_junk.csv 
	time perl aveP.pl eclipse.dupes < eclipse_norep_junk.csv | tee eclipse_norep_junk_map.txt
eclipse_norep_nfr_map.txt: eclipse.dupes eclipse_norep_nfr.csv 
	time perl aveP.pl eclipse.dupes < eclipse_norep_nfr.csv | tee eclipse_norep_nfr_map.txt

eclipse_all_map.txt: eclipse.dupes eclipse_all.csv 
	time perl aveP.pl eclipse.dupes < eclipse_all.csv | tee eclipse_all_map.txt

eclipse_all_norep_map.txt: eclipse.dupes eclipse_all_norep.csv 
	time perl aveP.pl eclipse.dupes < eclipse_all_norep.csv | tee eclipse_all_norep_map.txt





mozilla_rep.csv: header_mozilla_cosines.csv
	time bash -x mklogit.sh header_mozilla_cosines.csv mozilla_rep.csv REP


mozilla_rep_map.txt: mozilla.dupes mozilla_rep.csv 
	time perl aveP.pl mozilla.dupes < mozilla_rep.csv | tee mozilla_rep_map.txt

mozilla_cosine_arch_map.txt: mozilla.dupes mozilla_cosines_arch.csv 
	time perl aveP.pl mozilla.dupes < mozilla_cosines_arch.csv | tee mozilla_cosine_arch_map.txt
mozilla_cosine_lda_map.txt: mozilla.dupes mozilla_cosines_lda.csv 
	time perl aveP.pl mozilla.dupes < mozilla_cosines_lda.csv | tee mozilla_cosine_lda_map.txt
mozilla_cosine_junk_map.txt: mozilla.dupes mozilla_cosines_junk.csv 
	time perl aveP.pl mozilla.dupes < mozilla_cosines_junk.csv | tee mozilla_cosine_junk_map.txt
mozilla_cosine_nfr_map.txt: mozilla.dupes mozilla_cosines_nfr.csv 
	time perl aveP.pl mozilla.dupes < mozilla_cosines_nfr.csv | tee mozilla_cosine_nfr_map.txt

mozilla_norep_arch_map.txt: mozilla.dupes mozilla_norep_arch.csv 
	time perl aveP.pl mozilla.dupes < mozilla_norep_arch.csv | tee mozilla_norep_arch_map.txt
mozilla_norep_lda_map.txt: mozilla.dupes mozilla_norep_lda.csv 
	time perl aveP.pl mozilla.dupes < mozilla_norep_lda.csv | tee mozilla_norep_lda_map.txt
mozilla_norep_junk_map.txt: mozilla.dupes mozilla_norep_junk.csv 
	time perl aveP.pl mozilla.dupes < mozilla_norep_junk.csv | tee mozilla_norep_junk_map.txt
mozilla_norep_nfr_map.txt: mozilla.dupes mozilla_norep_nfr.csv 
	time perl aveP.pl mozilla.dupes < mozilla_norep_nfr.csv | tee mozilla_norep_nfr_map.txt

mozilla_all_map.txt: mozilla.dupes mozilla_all.csv 
	time perl aveP.pl mozilla.dupes < mozilla_all.csv | tee mozilla_all_map.txt

mozilla_all_norep_map.txt: mozilla.dupes mozilla_all_norep.csv 
	time perl aveP.pl mozilla.dupes < mozilla_all_norep.csv | tee mozilla_all_norep_map.txt




map.txt: eclipsemap.txt mozillamap.txt
	cat eclipsemap.txt mozillamap.txt > map.txt


eclipsemap.txt: eclipse_rep_map.txt  eclipse_cosine_arch_map.txt eclipse_cosine_lda_map.txt eclipse_cosine_junk_map.txt eclipse_cosine_nfr_map.txt eclipse_norep_arch_map.txt eclipse_norep_lda_map.txt eclipse_norep_junk_map.txt eclipse_norep_nfr_map.txt eclipse_all_map.txt eclipse_all_norep_map.txt
	fgrep "" eclipse_rep_map.txt  eclipse_cosine_arch_map.txt eclipse_cosine_lda_map.txt eclipse_cosine_junk_map.txt eclipse_cosine_nfr_map.txt eclipse_norep_arch_map.txt eclipse_norep_lda_map.txt eclipse_norep_junk_map.txt eclipse_norep_nfr_map.txt eclipse_all_map.txt eclipse_all_norep_map.txt  > eclipsemap.txt


mozillamap.txt: mozilla_rep_map.txt  mozilla_cosine_arch_map.txt mozilla_cosine_lda_map.txt mozilla_cosine_junk_map.txt mozilla_cosine_nfr_map.txt mozilla_norep_arch_map.txt mozilla_norep_lda_map.txt mozilla_norep_junk_map.txt mozilla_norep_nfr_map.txt mozilla_all_map.txt mozilla_all_norep_map.txt
	fgrep "" mozilla_rep_map.txt  mozilla_cosine_arch_map.txt mozilla_cosine_lda_map.txt mozilla_cosine_junk_map.txt mozilla_cosine_nfr_map.txt mozilla_norep_arch_map.txt mozilla_norep_lda_map.txt mozilla_norep_junk_map.txt mozilla_norep_nfr_map.txt mozilla_all_map.txt mozilla_all_norep_map.txt > mozillamap.txt

