#!/bin/bash
FILE=$1
OUT=$2
COLUMN=$3
NOREP=$4
CONTEXT=$5
COEF="$FILE.8020.$COLUMN.$NOREP.$CONTEXT.lmfit-coef.csv"
SAMPLE="$FILE.8020.$COLUMN.$NOREP.$CONTEXT"
bash 8020.sh "$FILE" > $SAMPLE
Rscript logistic.R $SAMPLE $COLUMN $COEF $NOREP $CONTEXT
cat logitheader.csv > $OUT
# sort reveresed, use 512M of ram, use the 9th column, sort by numbers , is the seperator
mkdir sorted
perl logit.pl "$FILE" "$COLUMN" "$COEF" $NOREP $CONTEXT  | sort -r -S 512M -T ./sorted -t, -n -k 9 >> $OUT


