file = "header_eclipse_cosines.csv"
column = "cosine_architecture"
outfile = "lmfit-coef.csv"
norep = 0
allcontext = 0
args<-commandArgs(TRUE)

file = if (length(args) >= 1) { args[1] } else { file }
column = if (length(args) >= 2) { args[2] } else { column }
outfile = if (length(args) >= 3) { args[3] } else { outfile }
norep = if (length(args) >= 4) { args[4] } else { norep }
allcontext = if (length(args) >= 5) { args[5] } else { allcontext }


print(paste(file,column))
v <- read.csv(file)
lmfit <- if (column == "REP") {
  glm(paste("class == \"dup\" ~  REP "), data = v, family="binomial")
} else if (allcontext > 0 && norep == 0) {
  glm(paste("class == \"dup\" ~ REP + cosine_architecture + cosine_nfr + cosine_lda"), data = v, family="binomial")  
} else if (allcontext > 0 && norep > 0) {
  glm(paste("class == \"dup\" ~ cosine_architecture + cosine_nfr + cosine_lda"), data = v, family="binomial")  
} else if (norep > 0) {
  glm(paste("class == \"dup\" ~ ",column), data = v, family="binomial")
} else {
  glm(paste("class == \"dup\" ~  REP + ",column), data = v, family="binomial")
}
print(lmfit$coefficients)




write.table(lmfit$coefficients,"lmfit-coef.txt")
write.csv(lmfit$coefficients,"lmfit-coef.csv")
write.csv(lmfit$coefficients,outfile)

