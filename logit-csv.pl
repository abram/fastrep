#!/usr/bin/perl
use strict;
use Text::CSV_XS;

my ($inputfile, $column, $lmfit) = @ARGV;
$lmfit ||= "lmfit-coef.csv";
$column ||= "cosine_architecture";
$inputfile ||= "header_eclipse_cosines.csv";

# get the coeffecients

open my $fh, "<:encoding(utf8)", "lmfit-coef.csv" or die "test.csv: $!";
my $csv = Text::CSV_XS->new;
my @b = ();
my $first=0;
while ( my $row = $csv->getline( $fh ) ) {
    push @b, $row->[1] unless !$first++;
}
$csv->eof or $csv->error_diag();
close $fh;
warn join("\t",@b);
# premature evaluation
eval "sub z {  $b[0] + $b[1] * \$_[0] + $b[2] * \$_[1] }";

warn logit(0,1);
warn logit(1,0);
warn logit(0,0);

# now 

open my $fh, "<:encoding(utf8)", $inputfile or die "$inputfile: $!";
my $header = $csv->getline( $fh );
my @header = @$header;
#print join(",",@header,"logit"),$/;
my %header = ();
{
    my $i = 0;
    %header = map { $_ => $i++; } @header;
}
my $repc = $header{REP};
my $c = $header{$column};

sub logit { exp($_[0])/(1+exp($_[0])) }

while ( my $row = $csv->getline( $fh ) ) {
    print join(",",@$row, logit(z($row->[$repc],$row->[$c]))),$/;
}
