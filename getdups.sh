#!/bin/bash
COSINE_CSV=$1
DUPEFILE=$2
awk -F, '{if ($3=="dup") { print $1 "\n" $2}}' $COSINE_CSV | sort -u > $DUPEFILE
