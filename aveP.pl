#!/usr/bin/perl
use strict;
use List::Util qw(sum);
use Fatal qw(open close);
my $DUPES = shift @ARGV || "eclipse.dupes";
open(my $fd, $DUPES);
my @dupes = <$fd>;
close($fd);
chomp(@dupes);
my %dupes = map { $_ => { seen => 0, hits => 0, q => [] } } @dupes;
my $header = <>;
my $cnt = 0;
while(my $line = <>) {
    my ($id1,$id2,$class) = split(/,/,$line);
    # https://en.wikipedia.org/wiki/Mean_average_precision#Mean_average_precision
    for my $id ($id1,$id2) {
        if (exists $dupes{$id}) {
            $dupes{$id}->{seen}++; #seen! rank
            if ($class eq 'dup') {
                $dupes{$id}->{hits}++; #how many have we seen before this?
                push @{$dupes{$id}->{q}},
                  $dupes{$id}->{hits} / (1.0 * $dupes{$id}->{seen});
                # rationale is that |relevant| /\ |retrieved| 
                #                   --------------------
                #                       |retrieved|
            }
        }
    }
    if ($cnt++%1000000 == 0) {
        warn $cnt;
    }
}

# now we've seen all the lines we should be able to calculate mean precision
# computes AveP per query
my @means = map {
    my $id = $_;
    my @l = @{$dupes{$id}->{q}};
    my $mean = sum(@l)/(1.0*scalar(@l));
} @dupes;
# compute MAP as mean of AveP
my $mean = sum(@means)/(1.0*scalar(@means));
print "MAP: $mean$/";
open(my $fd,">", $DUPES.time());
print $fd $_,$/ foreach @means;
close($fd);
